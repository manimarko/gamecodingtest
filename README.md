# How to run

You can check out the online version of the project at:
 
https://gamecodingtest-nikita.herokuapp.com/

or you can run project on you local machine:

`npm run server` - Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.
The page will reload if you make edits.

`npm run build` - Builds the app for production