declare var require;
const FontFaceObserver = require("fontfaceobserver");

export class FontLoader {

	static async load(data: Array<any>): Promise<any> {
		await Promise.all(data.map(item => new FontFaceObserver(item.name, item.settings).load()));
	}

}