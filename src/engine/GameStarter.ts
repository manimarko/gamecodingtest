import {IGameSettings} from "./IGameSettings";
import {Render} from "./Render";
import {InputController} from "./InputController";
import {wait} from "./Utils";
import {FontLoader} from "./FontLoader";
import {ServiceLocator} from "./ServiceLocator";
import {mediatorsMap} from "./animation/AnimatedView";
import {ViewManager} from "./ViewManager";

export class GameStarter {
	async start(settings: IGameSettings): Promise<any> {
		Render.init(settings.render);

		InputController.init();

		Render.stage.addChild(ViewManager.sceneRoot);

		Render.addTicker(ViewManager.update, ViewManager);
		Render.onResize(ViewManager.resize.bind(ViewManager));

		settings.di.forEach(model => {
			ServiceLocator.bindTo(model);
		});

		Object.keys(settings.mediatorsMap).forEach(key => {
			mediatorsMap[key] = settings.mediatorsMap[key];
		});

		await FontLoader.load(settings.fonts);

		await settings.prepareAction.execute();

		const gameLoop = settings.gameLoop;

		while (true) {
			await gameLoop.execute();
			await wait(100);
		}
	}
}