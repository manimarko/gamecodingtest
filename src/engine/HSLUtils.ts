export class HSLUtils {
	protected static hue2rgb(p, q, t) {
		if (t < 0)
			t += 1;
		if (t > 1)
			t -= 1;
		if (t < 1 / 6)
			return p + (q - p) * 6 * t;
		if (t < 1 / 2)
			return q;
		if (t < 2 / 3)
			return p + (q - p) * (2 / 3 - t) * 6;
		return p;
	}

	public static hexToHsl(color) {
		var r = ((color >> 16) & 255) / 255;
		var g = ((color >> 8) & 255) / 255;
		var b = (color & 255) / 255;
		var max = Math.max(r, g, b), min = Math.min(r, g, b);
		var h, s, l = (max + min) / 2;
		if (max == min) {
			h = s = 0; // achromatic
		} else {
			var d = max - min;
			s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
			switch (max) {
				case r:
					h = (g - b) / d + (g < b ? 6 : 0);
					break;
				case g:
					h = (b - r) / d + 2;
					break;
				case b:
					h = (r - g) / d + 4;
					break;
			}
			h /= 6;
		}
		return {h: h, s: s, l: l};
	}

	public static hslToHex(h, s, l) {
		var r, g, b;
		if (s === 0) {
			r = g = b = l; // achromatic
		} else {
			var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
			var p = 2 * l - q;
			r = HSLUtils.hue2rgb(p, q, h + 1 / 3);
			g = HSLUtils.hue2rgb(p, q, h);
			b = HSLUtils.hue2rgb(p, q, h - 1 / 3);
		}
		return r * 255 << 16 ^ g * 255 << 8 ^ b * 255;
	}

	public static addLightness(color, v) {
		const obj = this.hexToHsl(color);
		obj.l += v;
		return this.hslObjToHex(obj);
	}

	public static hslObjToHex(obj) {
		return HSLUtils.hslToHex(obj.h, obj.s, obj.l);
	}
}
