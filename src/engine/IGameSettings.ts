import {ICommand} from "./command/Command";

export interface IGameSettings {
	render: any;
	di: any[];
	prepareAction: ICommand;
	gameLoop: ICommand;
	mediatorsMap: any;
	fonts: any[];
}
