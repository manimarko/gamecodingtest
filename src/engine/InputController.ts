export class InputController {
    static get enabled(): boolean {
        return this._enabled;
    }

    static set enabled(value: boolean) {
        this._enabled = value;
        this.pressed = [];
        this.update();
    }
    static pressed = [];
    private static _enabled: boolean = false;

    static init() {
        document.addEventListener('keydown', this.onKeyDown.bind(this));
        document.addEventListener('keyup', this.onKeyUp.bind(this));
    }

    static onKeyDown(e: KeyboardEvent) {
        if (this._enabled) {
            if (e.key === "ArrowUp" || e.code === "KeyW") {
                this.pressed.push("up");
            }
            if (e.key === "ArrowDown" || e.code === "KeyS") {
                this.pressed.push("down");
            }
            if (e.key === "ArrowLeft" || e.code === "KeyA") {
                this.pressed.push("left");
            }
            if (e.key === "ArrowRight" || e.code === "KeyD") {
                this.pressed.push("right");
            }
            InputController.update();
        }
    }

    static onKeyUp(e: KeyboardEvent) {
        if (this._enabled) {
            if (e.key === "ArrowUp" || e.code === "KeyW") {
                this.pressed = this.pressed.filter(item => item !== "up");
            }
            if (e.key === "ArrowDown" || e.code === "KeyS") {
                this.pressed = this.pressed.filter(item => item !== "down");
            }
            if (e.key === "ArrowLeft" || e.code === "KeyA") {
                this.pressed = this.pressed.filter(item => item !== "left");
            }
            if (e.key === "ArrowRight" || e.code === "KeyD") {
                this.pressed = this.pressed.filter(item => item !== "right");
            }
            InputController.update();
        }
    }

    static update(): void {
        if (this.pressed.length > 0) {
            const last = this.pressed[this.pressed.length - 1];
            if (last === "up") {
                this.dir = 1;
                this.axis = "y";
            }
            if (last === "down") {
                this.dir = -1;
                this.axis = "y";
            }
            if (last === "left") {
                this.dir = -1;
                this.axis = "x";
            }
            if (last === "right") {
                this.dir = 1;
                this.axis = "x";
            }
        } else {
            this.dir = 0;
            this.axis = "";
        }
    }

    static axis: string;
    static dir = 0;
}