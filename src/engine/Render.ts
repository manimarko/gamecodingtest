import $ = require("jquery");

export class Render {
	private static screenSizeRangeX: Array<number>;
	private static screenSizeRangeY: Array<number>;
	private static parentHTML: HTMLElement;
	private static resizeCallbacks = [];

	public static pixiApp: PIXI.Application;

	static get stage(): PIXI.Container {
		return this.pixiApp.stage;
	}

	static get screenWidth(): number {
		return this.pixiApp.renderer.screen.width;
	}

	static get screenHeight(): number {
		return this.pixiApp.renderer.screen.height;
	}

	static init(data: IRenderData) {
		this.screenSizeRangeX = data.screenSizeRangeX;
		this.screenSizeRangeY = data.screenSizeRangeY;
		this.parentHTML = data.parentHTML;
		this.pixiApp = new PIXI.Application({
			width: data.screenSizeRangeX[0],
			height: data.screenSizeRangeY[0],
			antialias: true,
			resolution: 4,
			backgroundColor: data.bgColor
		});

		$(this.pixiApp.view).css("border", "0");
		$(this.pixiApp.view).css("overflow", "hidden");
		$(this.pixiApp.view).css("border-style", "none");
		$(this.pixiApp.view).css("display", "inline-block");

		window.addEventListener("resize", () => {
			this.resize();
		});

		this.addTicker(() => {
			scrollTo(0, 0);
			moveTo(0, 0);
		})

		this.parentHTML.appendChild(this.pixiApp.view);
		this.resize();
	}

	protected static resize(): void {
		let minScreenW = this.screenSizeRangeX[0];
		let maxScreenW = this.screenSizeRangeX[1];
		let minScreenH = this.screenSizeRangeY[0];
		let maxScreenH = this.screenSizeRangeY[1];

		if (this.pixiApp.renderer) {
			let windowWidth = $(window).width();
			let windowHeight = $(window).height();

			const maxAspect = maxScreenW / minScreenH;
			const minAspect = minScreenW / maxScreenH;

			let containerWidth = windowWidth;

			let windowRatio = windowWidth / windowHeight;

			if (windowRatio < minAspect) {
				windowRatio = minAspect;
				containerWidth = windowWidth;
			} else if (windowRatio > maxAspect) {
				windowRatio = maxAspect;
				containerWidth = windowHeight * windowRatio;
			}

			let containerHeight = containerWidth / windowRatio;

			$(this.pixiApp.view).css("height", containerHeight + "px");
			$(this.pixiApp.view).css("width", containerWidth + "px");
			$(this.pixiApp.view).css("margin-left", Math.floor((windowWidth - containerWidth) * 0.5) + "px");
			$(this.pixiApp.view).css("margin-top", Math.floor((windowHeight - containerHeight) * 0.5) + "px");

			let aspectRatio = (maxAspect - minAspect) > 0 ? (windowRatio - minAspect) / (maxAspect - minAspect) : minAspect;

			let renderWidth = minScreenW + (maxScreenW - minScreenW) * aspectRatio;
			let renderHeight = renderWidth / windowRatio;

			this.pixiApp.renderer.resize(renderWidth, renderHeight);

			this.resizeCallbacks.forEach(callback => callback(renderWidth, renderHeight));
		}
	}

	static addTicker(func, context = null) {
		this.pixiApp.ticker.add((delta) => {
			let time = PIXI.Ticker.system.elapsedMS;
			let count = 5;
			while (time >= 16 && count > 0) {
				func.call(context, 16 * 0.001);
				time -= 16;
				count--;
			}
		});
	}

	static removeTicker(func) {
		this.pixiApp.ticker.remove(func);
	}

	static onResize(func) {
		this.resizeCallbacks.push(func);
	}
}

export interface IRenderData {
	parentHTML: HTMLElement;
	screenSizeRangeX: Array<number>;
	screenSizeRangeY: Array<number>;
	bgColor: number;
}
