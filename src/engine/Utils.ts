import {HSLUtils} from "./HSLUtils";
import {ServiceLocator} from "./ServiceLocator";
import EventEmitter = PIXI.utils.EventEmitter;

export const wait = (time) => {
	return new Promise((resolve) => {
		setTimeout(resolve, time);
	});
};

export const waitEvent = (event) => {
	return new Promise((resolve) => {
		ServiceLocator.inject(EventEmitter).once(event, (e) => {
			resolve(e);
		});
	});
};

export const waitValue = (valueKey, value) => {
	return waitEvent(valueKey.toString() + ":" + value.toString())
};

export const dispatch = (event, ...data) => {
	console.log(event);
	(<EventEmitter>ServiceLocator.inject(EventEmitter)).emit(event, ...data);
}

export const listen = (event, func, context = null) => {
	ServiceLocator.inject(EventEmitter).on(event, func, context);
	//EventsMap
}

export const unsubscribe = (event, func) => {
	ServiceLocator.inject(EventEmitter).off(event, func);
}

export const range = (a, b) => {
	return a + (b - a) * Math.random();
}

const getRandomizedValue = (data: any, key: string): number => {
	if (data[key] instanceof Array) {
		return range(data[key][0], data[key][1]);
	}
	return data[key];
}

export const generateItems = (data: any, count: number): any[] => {
	const arr = [];
	const keys = Object.keys(data).filter(key => key !== "path" && key !== "key");
	for (let i = 0; i < count; i++) {

		const obj = {
			path: data.path.map(item => item.replace("{i}", i)),
			key: data.key,
		};

		keys.forEach(key => {
			obj[key] = getRandomizedValue(data, key);
		});
		//console.log(obj);

		arr.push(obj);
	}
	return arr;
}

export const generateStructureItems = (data: any, count: number, generateFunc: any = null): any[] => {
	const arr = [];
	const keys = Object.keys(data).filter(key => key !== "path");
	for (let i = 0; i < count; i++) {

		const obj = {
			path: data.path.replace("{i}", i),
		};

		keys.forEach(key => {
			obj[key] = getRandomizedValue(data, key);
		});

		if (generateFunc) {
			const objGen = generateFunc(i);
			Object.keys(objGen).forEach(key => {
				obj[key] = objGen[key];
			});
		}
		//console.log(obj);

		arr.push(obj);
	}
	return arr;
}
