import {AnimationMixer} from "./AnimationMixer";
import {Render} from "../Render";
import {textStyles} from "../../game/Constants";

export const mediatorsMap = {};

export class AnimatedView extends PIXI.Container {

	animator: AnimationMixer = new AnimationMixer();
	sequencePlayer: SequencePlayer = new SequencePlayer();
	mapData: null;

	constructor(data, mapData = null) {
		super();
		this.mapData = mapData;
		this.init(data, mapData);
		this.addListener("added", this.onAdded);
		this.addListener("removed", this.onRemoved);
	}

	addMediators(itemName, obj, mapData) {
		const mediatorClasses: any[] = mediatorsMap[itemName];
		if (mediatorClasses) {
			mediatorClasses.forEach(mediatorClass => {
				const mediator = new mediatorClass();
				mediator.mapData = mapData;
				mediator.init(obj);
			});
		}
	}

	onAdded(parent: PIXI.Container) {
		Render.addTicker(this.update, this);
		this.addMediators(this.name, this, this.mapData);
	}

	onRemoved(parent: PIXI.Container) {
		Render.removeTicker(this.update);
	}

	init(data, mapData): void {
		this.animator.clip = this;
		this.sequencePlayer.animator = this.animator;
		this.createStructure(data.structure, mapData);
		this.addAnimations(data.animations);
	}

	getChild(name): PIXI.DisplayObject {
		return this.getChildRecursive(name, this);
	}

	getChildRecursive(name: string, container: PIXI.Container): PIXI.DisplayObject {
		const child = container.getChildByName(name);
		if (child) {
			return child;
		} else {
			const children = container.children.filter(child => (<any>child).children);
			for (let i = 0; i < children.length; i++) {
				const child = this.getChildRecursive(name, children[i] as PIXI.Container);
				if (child) {
					return child;
				}
			}
		}
	}

	createPath(path: string[], data, mapData): PIXI.Container {
		let obj = this;
		path.forEach((itemName, i) => {
			if (itemName !== "") {
				const nextObj = (<any>obj.getChildByName(itemName));
				if (!nextObj) {
					let newObj = null;
					if (i === path.length - 1) {
						if (data.roundRect || data.circle || data.rect || data.ray || data.circleLine) {
							newObj = new PIXI.Graphics();
						} else if (data.image) {
							newObj = new PIXI.Sprite(null);
						} else if (data.text) {
							newObj = new PIXI.Text(data.text);
						} else if (data.image9) {
							newObj = new PIXI.NineSlicePlane(PIXI.Texture.WHITE, data.nWidth, data.nWidth, data.nWidth, data.nWidth);
						} else if (data.source) {
							newObj = new AnimatedView(data.source, mapData);
						} else {
							newObj = new PIXI.Container();
						}
					} else {
						newObj = new PIXI.Container();
					}
					newObj.name = itemName;
					obj.addChild(newObj);
					if (!(newObj instanceof AnimatedView)) {
						this.addMediators(newObj.name, newObj, mapData);
					}
					obj = newObj;
				} else {
					obj = nextObj;
				}
			}
		});

		return obj;
	}

	createStructure(structure, mapData): void {
		structure.forEach((data: IObjectData) => {

			const resultObject = this.createPath(data.path.split('.'), data, mapData);

			this.setParams(resultObject, data);
		});
	}

	setParams(obj: PIXI.Container, data: IObjectData): void {
		if (data.roundRect) {
			(obj as PIXI.Graphics).beginFill(data.roundRect.color, 1);
			(obj as PIXI.Graphics).drawRoundedRect(0, 0, data.roundRect.w, data.roundRect.h, data.roundRect.r);
			(obj as PIXI.Graphics).endFill();
		}

		if (data.circleLine) {
			(obj as PIXI.Graphics).lineStyle(data.circleLine.width, data.circleLine.color, 1);
			(obj as PIXI.Graphics).drawCircle(0, 0, data.circleLine.r);
		}

		if (data.ray) {
			(obj as PIXI.Graphics).beginFill(data.ray.color, 1);
			(obj as PIXI.Graphics).moveTo(0, 0);
			const angle1 = (data.ray.angle - data.ray.delta) * Math.PI * 2;
			const angle2 = (data.ray.angle + data.ray.delta) * Math.PI * 2;
			(obj as PIXI.Graphics).lineTo(Math.cos(angle1) * data.ray.len, Math.sin(angle1) * data.ray.len);
			(obj as PIXI.Graphics).lineTo(Math.cos(angle2) * data.ray.len, Math.sin(angle2) * data.ray.len);
			(obj as PIXI.Graphics).lineTo(0, 0);
			(obj as PIXI.Graphics).endFill();
		}
		if (data.rect) {
			(obj as PIXI.Graphics).beginFill(data.rect.color, 1);
			(obj as PIXI.Graphics).drawRect(0, 0, data.rect.w, data.rect.h);
			(obj as PIXI.Graphics).endFill();
		}
		if (data.circle) {
			(obj as PIXI.Graphics).beginFill(data.circle.color, 1);
			(obj as PIXI.Graphics).drawCircle(0, 0, data.circle.r);
			(obj as PIXI.Graphics).endFill();
		}

		if (data.image) {
			(obj as PIXI.Sprite).texture = PIXI.Texture.from(`images/${data.image}.png`);
		}

		if (data.image9) {
			(obj as PIXI.NineSlicePlane).texture = PIXI.Texture.from(`images/${data.image9}.png`);
		}

		if (data.text) {
			const tf = (obj as PIXI.Text);
			tf.style = textStyles[data.text.style];
			tf.text = data.text.text;
		}
		if (data.pos) {
			obj.x = data.pos[0];
			obj.y = data.pos[1];
		}
		if (data.width) {
			obj.width = data.width;
		}
		if (data.height) {
			obj.height = data.height;
		}
		if (data.angle) {
			obj.angle = data.angle;
		}
		if (data.scale) {
			obj.scale.x = data.scale[0];
			obj.scale.y = data.scale[1];
		}
		if (data.pivot) {
			obj.pivot.x = data.pivot[0];
			obj.pivot.y = data.pivot[1];
		}
		if (data.anchor) {
			(<any>obj).anchor.x = data.anchor[0];
			(<any>obj).anchor.y = data.anchor[1];
		}
		if (data.alpha) {
			obj.alpha = data.alpha;
		}

		if (data.interactive) {
			obj.interactive = data.interactive;
		}

		if (data.cache) {
			obj.cacheAsBitmap = true;
		}
	}

	addAnimations(animations): void {
		Object.keys(animations).forEach(animationName => {
			this.animator.addAnimation(animationName, animations[animationName]);
		})
	}

	update(dt: number): void {
		this.sequencePlayer.update(dt);
		this.animator.update(dt);
	}
}

export class SequencePlayer {
	sequence: ISequencePart[] = [];
	current: ISequencePart;
	animator: AnimationMixer;
	time: number = 0;

	playSequence(sequence: ISequencePart[]): void {
		this.sequence = sequence;
		this.nextSequenceAnimation();
	}

	nextSequenceAnimation(): void {
		if (this.sequence.length > 0) {
			const item = this.sequence.shift();
			const currAnimationData = {trackIndex: 0, blendTime: 0, ...item};
			this.time = 0;
			this.animator.setCurrentAnimation(currAnimationData.name, currAnimationData.trackIndex, currAnimationData.blendTime);
			this.current = currAnimationData;
		}
	}

	update(dt: number): void {
		if (this.current && this.sequence.length > 0) {
			this.time += dt;
			if (this.time > this.current.time) {
				this.nextSequenceAnimation();
			}
		}
	}

	get isCompleted(): boolean {
		return this.sequence.length === 0 && (!this.current || this.time > this.current.time);
	}
}

export interface ISequencePart {
	name: string,
	trackIndex?: number,
	time: number,
	blendTime?: number
}

export interface IRect {
	w: number,
	h: number,
	color: number
}

export interface IRoundRect {
	w: number,
	h: number,
	r: number,
	color: number
}

export interface ICircle {
	r: number,
	color: number
}

export interface ICircleLine extends ICircle {
	width: number
}

export interface IRay {
	angle: number,
	len: number,
	delta: number
	color: number
}

export interface IText {
	text: string,
	color: number,
	style: string
	align: string
}

export interface IObjectData {
	interactive: boolean;
	path: string;
	pos: number[];
	scale: number[];
	pivot: number[];
	anchor: number[];
	pivotAsPercent: boolean;
	angle: number;
	alpha: number;
	width: number;
	height: number;
	roundRect: IRoundRect;
	ray: IRay;
	text: IText;
	circle: ICircle;
	image: string;
	image9: string;
	circleLine: ICircleLine;
	rect: IRect;
	cache: boolean;
}