import {Animator} from './Animator';
import {AnimatorTrack} from "./AnimatorTrack";

export const getObject = (clip, path) => {
	let obj = clip;
	path.forEach(item => {
		if (item !== "") {
			obj = (<any>obj.getChildByName(item));
		}
	});
	return obj;
}

export class AnimationMixer {
	tracks: AnimatorTrack[] = [];
	clip: PIXI.Container;
	animations = {};
	childMap = {};

	addAnimation(name, data, scale = 1) {
		const animator = new Animator();
		animator.addAnimations(data);
		//animator.scale = scale;
		animator.animations.forEach(animation => {
			if (this.childMap[animation.uniq_key]) {
				return;
			}
			const currentObject = getObject(this.clip, animation.path);
			if (!currentObject) {
				throw new Error("invalid path = " + animation.path);
			}
			let beginValue = currentObject[animation.key];
			if (animation.key === "scaleY") {
				beginValue = currentObject.scale.y;
			}
			if (animation.key === "scaleX" || animation.key === "scaleXY") {
				beginValue = currentObject.scale.x;
			}
			this.childMap[animation.uniq_key] = {
				beginValue: beginValue,
				obj: currentObject,
				key: animation.key
			};
		});
		this.animations[name] = animator;
	}

	setCurrentAnimation(name, trackIndex = 0, blendTime = 0.1): void {
		if (!this.tracks[trackIndex]) {
			this.tracks[trackIndex] = new AnimatorTrack();
		}
		const track = this.tracks[trackIndex];
		track.maxBlendTime = blendTime;
		track.setCurrentAnimation(this.animations[name]);
	}

	protected actualizeValues(): void {
		this.tracks.forEach(track => {
			Object.keys(track.values).forEach(key => {
				const objData = this.childMap[key];
				if (objData.key === "alpha") {
					objData.obj[objData.key] = track.values[key];
				} else {
					if (objData.key === "scaleX") {
						objData.obj.scale.x = track.values[key];
					} else if (objData.key === "scaleY") {
						objData.obj.scale.y = track.values[key];
					} else if (objData.key === "scaleXY") {
						objData.obj.scale.y = objData.obj.scale.x = track.values[key];
					} else {
						objData.obj[objData.key] = objData.beginValue + track.values[key];
					}
				}
			});
		});
	}

	update(dt: number): void {
		this.tracks.forEach(track => track.update(dt));
		this.actualizeValues();
	}
}
