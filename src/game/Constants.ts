export const GRID_CELL_SIZE = 16;
export const SCREEN_SIZE = {x: 32, y: 18};

export const textStyles = {
	"small_description": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 18,
		wordWrap: true,
		lineHeight: 15,
		align: "center"
	}),

	"small": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		dropShadow: true,
		dropShadowBlur: 10,
		dropShadowAlpha: 1,
		fontSize: 12
	}),

	"middle": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 24
	}),

	"big": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 30,
		dropShadow: true,
		dropShadowBlur: 10,
		dropShadowAlpha: 1,
	}),

	"big_red": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "red",
		fontSize: 30,
		dropShadow: true,
		dropShadowBlur: 10,
		dropShadowAlpha: 1,
	}),
}