import {ICommand} from "../../engine/command/Command";
import {ViewManager} from "../../engine/ViewManager";
import {GameView} from "../scenes/GameView";

export class ShowGameSceneCommand implements ICommand {

	async execute(data?: any): Promise<void> {
		ViewManager.show(GameView);
		return new Promise((resolve) => {
		});
	}
}