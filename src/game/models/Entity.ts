import {GameModel} from "./GameModel";

export class Entity {
	behaviour: IEntityBehaviour;
	data: any;
	world: GameModel;
	view: PIXI.Container;

	init(): void {
		this.data.entity = this;
		this.view = this.behaviour.getView ? this.behaviour.getView(this.data) : null;
	}

	update(dt: number): void {
		this.behaviour.getActiveProcesses(this.data).forEach(visitor => visitor(this, dt, this.world));
	}

	updateView(dt: number): void {
		if (this.view) {
			this.behaviour.updateView(this.data, this.view, dt);
		}
	}
}

export interface IEntityBehaviour {
	ID: string;

	color?: number;

	getActiveProcesses(data): any[];

	getInitialData(): any;

	updateView(data, view: PIXI.Container, dt: number): void;

	getView(data): any;
}

export interface IEntityData extends Object {
	pos: { x: number, y: number },
	mustRemove: boolean
}