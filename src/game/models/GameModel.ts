import {Entity, IEntityBehaviour} from "./Entity";
import {AStarFinder} from "astar-typescript";
import {BlueBoxBehaviour} from "./behaviours/BlueBoxBehaviour";
import {RedBoxBehaviour} from "./behaviours/RedBoxBehaviour";
import {PlayerBoxBehaviour} from "./behaviours/PlayerBoxBehaviour";
import {WhiteBoxBehaviour} from "./behaviours/WhiteBoxBehaviour";

export class GameModel {
	static SIZE = {x: 64, y: 64};
	entities: Entity[] = [];
	behaviours: { [key: string]: IEntityBehaviour } = {};
	entityIndex = 0;
	aStarInstance: AStarFinder;
	gridEntities;

	constructor() {
		this.gridEntities = [];
		for (let i = 0; i < GameModel.SIZE.y; i++) {
			this.gridEntities.push(new Array(GameModel.SIZE.x));
		}
	}

	initializePathFinding(): void {
		let grid = [];
		for (let i = 0; i < GameModel.SIZE.y; i++) {
			grid.push(new Array(GameModel.SIZE.x));
		}
		let entities = this.getEntitiesById(BlueBoxBehaviour.ID);
		entities.forEach(entity => {
			grid[entity.data.pos.y][entity.data.pos.x] = 1;
		});

		this.aStarInstance = new AStarFinder({
			grid: {
				matrix: grid
			},
			diagonalAllowed: false,
			includeStartNode: false
		});
	}

	reset() {
		this.entities = [];
	}

	get isWon(): boolean {
		return this.getEntitiesById(RedBoxBehaviour.ID).length === 0;
	}

	get isLose(): boolean {
		let player = this.getEntitiesById(PlayerBoxBehaviour.ID)[0]
		return !player || this.getEntitiesById(RedBoxBehaviour.ID).length > (player.data.mines + this.getEntitiesById(WhiteBoxBehaviour.ID).length);
	}

	addEntity(behaviorID: string, data: any): Entity {
		const behavior = this.behaviours[behaviorID];
		const entity = new Entity();
		entity.behaviour = behavior;
		entity.data = {...behavior.getInitialData(), ...data};
		entity.data.id = behaviorID;
		entity.data.internalId = this.entityIndex.toString();
		entity.world = this;
		entity.init();
		this.entities.push(entity);
		this.entityIndex++;
		return entity;
	}

	addBehaviours(arr: IEntityBehaviour[]) {
		arr.forEach((item, i) => {
			this.behaviours[item.ID] = item;
		});
	}

	updateGrid(): void {
		for (let i = 0; i < this.gridEntities.length; i++) {
			let arr = this.gridEntities[i];
			for (let j = 0; j < arr.length; j++) {
				arr[j] = [];
			}
		}

		this.entities.forEach(entity => {
			this.gridEntities[entity.data.pos.y][entity.data.pos.x].push(entity);
		});
	}

	update(dt: number): void {
		this.updateGrid();
		this.entities.concat().forEach(entity => entity.update(dt));
		this.entities = this.entities.filter(entity => !entity.data.mustRemove);
	}

	getEntitiesById(id: string): Entity[] {
		return this.entities.filter(entity => entity.data.id === id);
	}

	getEntitiesByPos(pos: { x: number, y: number }): Entity[] {
		if (this.gridEntities && pos.x > -1 && pos.y > -1 && pos.y < GameModel.SIZE.y && pos.x < GameModel.SIZE.x) {
			return this.gridEntities[pos.y][pos.x];
		}
		return [];
	}
}