import {IEntityBehaviour} from "../Entity";
import {getBoxView, updateBoxView} from "./CommonStrategies";

export const BlueBoxBehaviour: IEntityBehaviour = {
	ID: "BlueBoxBehaviour",
	getActiveProcesses: (data) => [],

	getView: getBoxView,
	updateView: updateBoxView,

	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0x3366FF,
		};
	}
};