import {GRID_CELL_SIZE} from "../../Constants";

export function getBoxView(data): PIXI.Container {
	const view: PIXI.Graphics = new PIXI.Graphics();
	view.beginFill(data.color, 1);
	view.drawRect(0, 0, GRID_CELL_SIZE, GRID_CELL_SIZE);
	view.endFill();
	view.interactive = false;
	view.interactiveChildren = false;
	return view;
}

export function updateBoxView(data, view: PIXI.Graphics) {
	view.position.set(data.pos.x * GRID_CELL_SIZE, data.pos.y * GRID_CELL_SIZE);
}