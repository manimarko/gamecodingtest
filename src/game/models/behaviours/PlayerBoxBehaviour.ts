import {Entity, IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";
import {getBoxView, updateBoxView} from "./CommonStrategies";
import {WhiteBoxBehaviour} from "./WhiteBoxBehaviour";
import {GRID_CELL_SIZE} from "../../Constants";

export function updateDying(entity: Entity, dt: number, world: GameModel) {
	const data = entity.data;
	data.animationTime += dt;
	if (data.animationName === "die") {
		let k = data.animationTime / data.animations[data.animationName].max;
		if (k > 1) {
			data.lives--;
			world.getEntitiesByPos(data.pos).forEach(entity => entity.data.mustRemove = true);
			if (data.lives > 0) {
				data.mustRemove = false;
				data.mines = 0;
				data.animationName = "idle";
				data.animationTime = 0;
			}
		}
	}
}

export function moveToPoint(entity: Entity, dt: number, world: GameModel) {
	const data = entity.data;
	if (data.path && data.path.length > 0 && data.time > 1 / data.speed) {
		let obj = data.path.shift();
		data.pos.x = obj[0];
		data.pos.y = obj[1];
		data.time = 0;
	}
	data.time += dt;
}

export const collectWhiteBehaviour = (entity: Entity, dt: number, world: GameModel) => {
	let entities = world.getEntitiesByPos(entity.data.pos);
	const whiteBox = entities.find(entity => entity.data.id === WhiteBoxBehaviour.ID);
	if (whiteBox) {
		whiteBox.data.mustRemove = true;
		entity.data.mines++;
	}
}

export const PlayerBoxBehaviour: IEntityBehaviour = {
	ID: "PlayerBoxBehaviour",
	getActiveProcesses: (data) => {
		if (data.animationName === "die") {
			return [updateDying];
		}
		return [moveToPoint, collectWhiteBehaviour];
	},
	getView: getBoxView,
	updateView: (data, view: PIXI.Graphics) => {
		view.pivot.x = view.pivot.y = GRID_CELL_SIZE * 0.5;
		updateBoxView(data, view);
		view.x += GRID_CELL_SIZE * 0.5;
		view.y += GRID_CELL_SIZE * 0.5;
		if (data.animationName === "idle") {
			view.scale.x = 1;
			view.scale.y = 1;
			view.alpha = 1;
		}
		if (data.animationName === "die") {
			let k = data.animationTime / data.animations[data.animationName].max;
			view.scale.x = 1 + Math.abs(Math.sin(k * Math.PI * 4) * 0.2);
			view.scale.y = 1 + Math.abs(Math.sin(k * Math.PI * 5) * 0.2);
			view.alpha = Math.abs(Math.sin(k * Math.PI * 5))
		}
	},
	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0x66FF33,
			speed: 10,
			mines: 0,
			time: 0,
			animationName: "idle",
			animationTime: 0,
			animations: {idle: {max: 1}, die: {max: 3}},
			score: 0,
			lives: 3,
		};
	}
};