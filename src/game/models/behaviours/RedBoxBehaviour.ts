import {Entity, IEntityBehaviour} from "../Entity";
import {getBoxView, updateBoxView} from "./CommonStrategies";
import {GameModel} from "../GameModel";
import {collectWhiteBehaviour, moveToPoint, PlayerBoxBehaviour} from "./PlayerBoxBehaviour";
import {WhiteBoxBehaviour} from "./WhiteBoxBehaviour";

export function dist(a, b) {
	const dx = b.x - a.x;
	const dy = b.y - a.y;
	return Math.sqrt(dx * dx + dy * dy);
}

export const findEnemyBehaviour = (entity: Entity, dt: number, world: GameModel) => {
	const data = entity.data;
	if (!data.path || data.path.length != data.initialPathLength || !data.initialPathLength) {
		let pos = {x: 0, y: 0};
		let R = 5;
		let enemys = [];
		for (let i = -R; i <= R; i++) {
			for (let j = -R; j <= R; j++) {
				pos.x = data.pos.x + j;
				pos.y = data.pos.y + i;
				let entities = world.getEntitiesByPos(pos);
				enemys.push(...entities.filter(entity => entity.data.id === PlayerBoxBehaviour.ID || entity.data.id === WhiteBoxBehaviour.ID));
			}
		}

		let enemy = enemys.find(entity => entity.data.id === PlayerBoxBehaviour.ID);

		if (!enemy || enemy.data.animationName === "die") {
			enemys.sort((a, b) => dist(a.data.pos, data.pos) - dist(b.data.pos, data.pos));
			enemy = enemys[0];
		}

		if (enemy && world.aStarInstance) {
			data.path = world.aStarInstance.findPath(data.pos, enemy.data.pos);
			data.initialPathLength = data.path.length;
			return;
		}

		data.path = [];
		data.initialPathLength = 0;
	}
}

export const killPlayer = (entity: Entity, dt: number, world: GameModel) => {
	const data = entity.data;
	let entities = world.getEntitiesByPos(data.pos);
	let player = entities.find(entity => entity.data.id === PlayerBoxBehaviour.ID);
	if (player && player.data.animationName === "idle") {
		player.data.animationName = "die";
		player.data.animationTime = 0;
		data.mustRemove = true;
	}
}

export const RedBoxBehaviour: IEntityBehaviour = {
	ID: "RedBoxBehaviour",
	getActiveProcesses: (data) => {
		return [findEnemyBehaviour, moveToPoint, killPlayer, collectWhiteBehaviour];
	},
	getView: getBoxView,
	updateView: updateBoxView,
	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			speed: 5,
			color: 0xFF3311,
			time: 0,
			score: 100
		};
	}
};