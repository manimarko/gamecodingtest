import {IEntityBehaviour} from "../Entity";
import {getBoxView, updateBoxView} from "./CommonStrategies";

export const WhiteBoxBehaviour: IEntityBehaviour = {
	ID: "WhiteBoxBehaviour",
	getActiveProcesses: (data) => [],
	getView: getBoxView,
	updateView: updateBoxView,
	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0xFFFFFF,
		};
	}
};