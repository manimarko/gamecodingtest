import {Entity, IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";
import {getBoxView, updateBoxView} from "./CommonStrategies";
import {RedBoxBehaviour} from "./RedBoxBehaviour";
import {PlayerBoxBehaviour} from "./PlayerBoxBehaviour";

export function mineBehaviour(entity: Entity, dt: number, world: GameModel) {
	let entities = world.getEntitiesByPos(entity.data.pos);
	let enemysArray = entities.filter(entity => entity.data.id === RedBoxBehaviour.ID);
	if (enemysArray.length > 0) {
		enemysArray[0].data.mustRemove = true;
		entity.data.mustRemove = true;
		let player = world.getEntitiesById(PlayerBoxBehaviour.ID)[0];
		if (player) {
			player.data.score += enemysArray[0].data.score;
		}
	}
}

export const YellowBoxBehaviour: IEntityBehaviour = {
	ID: "YellowBoxBehaviour",
	getActiveProcesses: (data) => [mineBehaviour],
	getView: getBoxView,
	updateView: updateBoxView,
	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0xFFEE00,
		};
	}
};
