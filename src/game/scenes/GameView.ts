import {ServiceLocator} from '../../engine/ServiceLocator';
import {IBaseView} from "../../engine/ViewManager";
import {GameModel} from "../models/GameModel";
import {WhiteBoxBehaviour} from "../models/behaviours/WhiteBoxBehaviour";
import {PlayerBoxBehaviour} from "../models/behaviours/PlayerBoxBehaviour";
import {BlueBoxBehaviour} from "../models/behaviours/BlueBoxBehaviour";
import {YellowBoxBehaviour} from "../models/behaviours/YellowBoxBehaviour";
import {Entity} from "../models/Entity";
import {RedBoxBehaviour} from "../models/behaviours/RedBoxBehaviour";
import {GRID_CELL_SIZE, SCREEN_SIZE} from "../Constants";
import {AnimatedView} from "../../engine/animation/AnimatedView";
import {GUIViewData} from "../viewData/GUIViewData";

export class GameView extends PIXI.Container implements IBaseView {
	showData: any;
	priority: number = 0;
	gameModel: GameModel = ServiceLocator.inject(GameModel);
	gr: PIXI.Graphics = new PIXI.Graphics();
	cont: PIXI.Container = new PIXI.Container();
	contEntities: PIXI.Container = new PIXI.Container();
	xIndex = 0;
	yIndex = 0;
	player: Entity;
	gui: AnimatedView;

	draw(count_x: number, count_y: number): void {
		this.gr.beginFill(0x444444, 1);
		this.gr.drawRect(0, 0, count_x * GRID_CELL_SIZE, count_y * GRID_CELL_SIZE);
		this.gr.endFill();
		this.gr.lineStyle(0.1, 0xFFFFFF);
		for (let i = 0; i < count_y; i++) {
			this.gr.moveTo(GRID_CELL_SIZE * i, 0);
			this.gr.lineTo(GRID_CELL_SIZE * i, GRID_CELL_SIZE * count_x);
		}
		for (let i = 0; i < count_x; i++) {
			this.gr.moveTo(0, GRID_CELL_SIZE * i);
			this.gr.lineTo(GRID_CELL_SIZE * count_y, GRID_CELL_SIZE * i);
		}
		this.gr.lineStyle(5, 0xFF0000);
		this.gr.drawRect(0, 0, GRID_CELL_SIZE * count_x, GRID_CELL_SIZE * count_y);
	}

	onAdded(): void {
		this.gr.clear();
		this.addChild(this.cont);
		this.cont.addChild(this.gr);
		this.cont.addChild(this.contEntities);
		this.draw(64, 64);
		this.cont.x = 0;
		this.cont.y = 0;

		this.gr.on('click', this.onClick.bind(this));
		this.gr.on('tap', this.onClick.bind(this));
		this.gr.interactive = this.gr.interactiveChildren = true;
		document.addEventListener('keydown', this.onSpaceBar.bind(this));

		let gui = new AnimatedView(GUIViewData, this.showData);
		this.gui = gui;
		this.addChild(gui);

		this.gameModel.addBehaviours([WhiteBoxBehaviour, PlayerBoxBehaviour, BlueBoxBehaviour, YellowBoxBehaviour, RedBoxBehaviour]);

		let freeSpaceGrid = [];
		for (let i = 0; i < GameModel.SIZE.y; i++) {
			freeSpaceGrid.push(new Array(GameModel.SIZE.x).fill(1));
		}

		for (let i = 0; i < GameModel.SIZE.x; i += 4) {
			for (let j = 0; j < GameModel.SIZE.y; j += 4) {
				let entity = this.gameModel.addEntity(WhiteBoxBehaviour.ID, {
					pos: {
						x: i,
						y: j
					}
				});
				this.fillGridRect(freeSpaceGrid, entity.data.pos.x, entity.data.pos.y, 1, 1, 0);
			}
		}

		for (let i = 0; i < 5; i++) {
			for (let j = 0; j < 2; j++) {
				let entity = this.gameModel.addEntity(RedBoxBehaviour.ID, {
					pos: {
						x: j * 4 + SCREEN_SIZE.x - 2,
						y: i * 5 + SCREEN_SIZE.y + 4,
					}
				});
				this.fillEntitySpace(freeSpaceGrid, entity.data.pos);
			}
		}
		this.player = this.gameModel.addEntity(PlayerBoxBehaviour.ID, {pos: {x: 1, y: 1}});
		this.fillEntitySpace(freeSpaceGrid, this.player.data.pos);
		this.fillBlue(freeSpaceGrid);
		this.gameModel.initializePathFinding();
		this.update(0);
	}

	fillEntitySpace(grid: any[][], pos, size: number = 8): void {
		this.fillGridRect(grid, pos.x - size * 0.5, pos.y - size * 0.5, size, size, 0);
	}

	fillGridRect(grid: any[][], x: number, y: number, w: number, h: number, v): void {
		for (let i = y; i < y + h; i++) {
			for (let j = x; j < x + w; j++) {
				if (i > -1 && i < grid.length && j > -1 && j < grid[i].length) {
					grid[i][j] = v;
				}
			}
		}
	}

	extractFreeItems(grid: any[][]): any[] {
		let items = [];
		for (let i = 0; i < grid.length; i++) {
			for (let j = 0; j < grid[i].length; j++) {
				if (grid[i][j]) {
					items.push({x: j, y: i});
				}
			}
		}
		return items;
	}

	fillBlue(grid: any[][]): void {
		let count = 32;
		let items = this.extractFreeItems(grid);
		while (count > 0 && items.length > 0) {
			let pos = items[Math.floor(items.length * Math.random())];
			this.gameModel.addEntity(BlueBoxBehaviour.ID, {
				pos: pos
			});
			this.fillEntitySpace(grid, pos);
			items = this.extractFreeItems(grid);
			count--;
		}
		console.log(count);
	}

	onClick(eventData) {
		let x = Math.floor(eventData.data.global.x / GRID_CELL_SIZE) + this.xIndex;
		let y = Math.floor(eventData.data.global.y / GRID_CELL_SIZE) + this.yIndex;

		this.player.data.path = this.player.world.aStarInstance.findPath(this.player.data.pos, {x: x, y: y});
	}

	onSpaceBar(e: KeyboardEvent) {
		if (this.gameModel.getEntitiesByPos(this.player.data.pos).length === 1 && this.player.data.mines > 0) {
			this.gameModel.addEntity(YellowBoxBehaviour.ID, {pos: {...this.player.data.pos}});
			this.player.data.mines--;
		}
	}

	onRemoved(): void {

	}

	update(dt: number): void {
		this.contEntities.removeChildren();
		this.gameModel.update(dt);
		this.gameModel.entities.forEach(entity => {
			entity.updateView(dt);
			this.contEntities.addChild(entity.view);
		});

		this.xIndex = this.player.data.pos.x - SCREEN_SIZE.x * 0.5;
		this.yIndex = this.player.data.pos.y - SCREEN_SIZE.y * 0.5;

		this.xIndex = Math.min(Math.max(0, this.xIndex), GameModel.SIZE.x - SCREEN_SIZE.x);
		this.yIndex = Math.min(Math.max(0, this.yIndex), GameModel.SIZE.y - SCREEN_SIZE.y);

		this.cont.x += (-this.xIndex * GRID_CELL_SIZE - this.cont.x) * 0.05;
		this.cont.y += (-this.yIndex * GRID_CELL_SIZE - this.cont.y) * 0.05;
		(<PIXI.Text>this.gui.getChild("lives")).text = `Lives: ${this.player.data.lives}`;
		(<PIXI.Text>this.gui.getChild("score")).text = `Score: ${this.player.data.score}`;
		(<PIXI.Text>this.gui.getChild("mines")).text = `Mines: ${this.player.data.mines}`;
		if (this.gameModel.isLose) {
			this.gui.animator.setCurrentAnimation("lose", 0, 0.5);
		} else if (this.gameModel.isWon) {
			this.gui.animator.setCurrentAnimation("won", 0, 0.5);
		} else {
			this.gui.animator.setCurrentAnimation("idle", 0, 0.5);
		}
	}

	resize(w: number, h: number): void {
		this.gui.x = w * 0.5;
	}

}