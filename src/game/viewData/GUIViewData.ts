export const GUIViewData = {
	structure: [
		{
			text: {text: "Lives:", style: "small"},
			anchor: [0.5, 0.5],
			pos: [0, 10],
			path: "lives"
		},
		{
			text: {text: "Mines:", style: "small"},
			anchor: [0.5, 0.5],
			pos: [-100, 10],
			path: "mines"
		},
		{
			text: {text: "Score:", style: "small"},
			anchor: [0.5, 0.5],
			pos: [100, 10],
			path: "score"
		},
		{
			text: {text: "You Won", style: "big"},
			anchor: [0.5, 0.5],
			pos: [0, 150],
			path: "won_label"
		},
		{
			text: {text: "You Died", style: "big_red"},
			anchor: [0.5, 0.5],
			pos: [0, 150],
			path: "lose_label"
		},
	],
	animations: {
		"won": [
			{path: ["won_label"], key: "alpha", period: 1, scale: 0.0, begin: 1},
			{path: ["lose_label"], key: "alpha", period: 1, scale: 0.0, begin: 0},
			{path: ["lose_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
			{path: ["won_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
		],
		"lose": [
			{path: ["won_label"], key: "alpha", period: 1, scale: 0.0, begin: 0},
			{path: ["lose_label"], key: "alpha", period: 1, scale: 0.0, begin: 1},
			{path: ["lose_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
			{path: ["won_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
		],
		"idle": [
			{path: ["won_label"], key: "alpha", period: 1, scale: 0.0, begin: 0},
			{path: ["lose_label"], key: "alpha", period: 1, scale: 0.0, begin: 0},
			{path: ["lose_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
			{path: ["won_label"], key: "scaleXY", period: 1, scale: 0.1, begin: 1},
		]
	}
};
