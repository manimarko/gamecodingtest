import {GameStarter} from "./engine/GameStarter";
import {PrepareGameCommand} from "./game/commands/PrepareGameCommand";
import {Sequence} from "./engine/command/Command";
import {GameModel} from "./game/models/GameModel";
import {ShowGameSceneCommand} from "./game/commands/ShowGameSceneCommand";

const gameStarter = new GameStarter();

gameStarter.start({
	render: {
		parentHTML: document.body,
		screenSizeRangeX: [16 * 32, 16 * 32],
		screenSizeRangeY: [16 * 18, 16 * 18],
		bgColor: 0x555555
	},
	di: [
		{classSource: GameModel, singleton: true, force: true},
	],
	mediatorsMap: {},
	fonts: [
		{name: 'Fredoka One', settings: {weight: 400}}
	],
	prepareAction: new PrepareGameCommand(),
	gameLoop: new Sequence(new ShowGameSceneCommand()),
});