const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
    let _plugins =
        [
            new CopyWebpackPlugin([
                {from: path.join(__dirname, './node_modules/pixi.js/dist/pixi.min.js'), to: ''},
                {from: path.join(__dirname, './node_modules/pixi.js/dist/pixi.min.js.map'), to: ''},
                {from: path.join(__dirname, './resources/images/*'), to: './images', flatten: true}
            ], {}),
            new HtmlWebpackPlugin({
                title: 'Custom template',
                // Load a custom template (lodash by default see the FAQ for details)
                template: './src/template.html'
            })
        ];

    return {
        entry: "./src/index.ts",

        devtool: "inline-source-map",
        plugins: _plugins,

        output: {
            path: path.join(__dirname, 'dist'),
            filename: 'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: [/node_modules/],
                }
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        devServer: {
            https: true
        }
    }

}